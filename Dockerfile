FROM asergiobranco/blackwingmicroservice:alpine
COPY ./ /home/bwmicroservice/dbmanager
RUN cd /home/bwmicroservice/dbmanager && python -m pip install -e ./
COPY docker/main.py /home/bwmicroservice/msdir/main.py
COPY docker/requirements.txt /home/bwmicroservice/msdir/requirements.txt
COPY docker/config.yaml /home/bwmicroservice/msdir/config.yaml
COPY docker/client /home/bwmicroservice/msdir/client