from ctypes import *

class DeviceInformation(Structure):
    _fields_ = [
        ("device_id", c_uint64),
        ("timestamp", c_uint64)
    ]

class Image(Structure):
    _fields_ = [
        ("image", (c_char * 10) * 8),
    ]

class Geolocation(Structure):
    _fields_ = [
        ("latitude", c_float),
        ("longitude", c_float),
        ("altitude", c_float),
        ("accuracy", c_float),
        ("altitude_accuracy", c_float),
        ("heading", c_float),
        ("speed", c_float),
    ]

class ImageAndGeolocation(Structure):
    _fields_ = [
        ("gps", Geolocation),
        ("image", Image),
        ("device", DeviceInformation)
    ]

class DeviceGeolocation(Structure):
    _fields_ = [
        ("gps", Geolocation),
        ("device", DeviceInformation)
    ]

class DeviceImage(Structure):
    _fields_ = [
        ("image", Image),
        ("device", DeviceInformation)
    ]


