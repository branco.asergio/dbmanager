from msgpack import ExtType

from bwmicroservice import BlackwingHandlerStreamer

from .parser import *

class DBManagerHandler(BlackwingHandlerStreamer):
    def setup(self):
        self.external_types = [GeolocationParser, ImageParser, GeolocationImageParser]

    def attendRequest(self):
        if isinstance(self.letter, ExtType):
            self.handle_external_type()
        elif isinstance(self.letter, dict):
            pass
        else:
            print("HERE!!!!")
            self.response = b'ERROR'
            self.last_message_recv()
        self.response = b'recv'
        self.parser.insert_into_collection()
            
    def handle_external_type(self):
        self.parser = self.external_types[self.letter.code](self.letter.data)