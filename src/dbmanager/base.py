from ctypes import Structure, Array
from pymongo import MongoClient 
from collections import ChainMap

class Base:
    def __init__(self, data : bytes, structure : Structure, database_name : str, collection : str):
        self.value = structure.from_buffer_copy(data)
        self.database_name = database_name
    
    def insert_into_collection(self):
        d = self.generate_dictionary()
        print(d)
        return
        client = MongoClient()
        client[self.database_name][self.collection_name].insert_one(d)
    
    def generic_parser(self, structure_data):
        d = {}
        for field in structure_data._fields_:
            field_name = field[0]
            d[field_name] = getattr(structure_data, field_name)
            if isinstance(d[field_name], Array):
                d[field_name] = self.unfold_this_array(field_name, d[field_name])
        return d
    
    def generate_dictionary(self):
        dictionaries = []
        for field in self.value._fields_:
            field_name = field[0]
            dictionaries.append(
                self.generic_parser(
                    getattr(self.value, field_name)
                )
            )
        return dict(ChainMap(*dictionaries))
