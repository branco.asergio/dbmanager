from ctypes import Structure
from .base import Base
from .externaltypes import *

DATABASE_NAME = "MyDB"

    
class GeolocationParser(Base):
    def __init__(self, data: bytes):
        super().__init__(data, DeviceGeolocation, DATABASE_NAME, "Geolocation")
    
class ImageParser(Base):
    def __init__(self, data: bytes):
        super().__init__(data, DeviceImage, DATABASE_NAME, "Image")
    
    def unfold_this_array(self, field_name, value):
        arr = [[0 for _ in range(10)] for _ in range(8)]
        for i in range(8):
            for j in range(10):
                arr[i][j] = value[i][j]
        return arr

class GeolocationImageParser(Base):
    def __init__(self, data: bytes):
        super().__init__(data, ImageAndGeolocation, DATABASE_NAME, "ImageGeolocated")
    def unfold_this_array(self, field_name, value):
        arr = [[0 for _ in range(10)] for _ in range(8)]
        for i in range(8):
            for j in range(10):
                arr[i][j] = value[i][j]
        return arr