import socket 
from msgpack import packb, unpackb, ExtType
from dbmanager.externaltypes import *
from random import randint, random
import time 
from ctypes import c_char

def generate_random_image():
    a = ((c_char*10)*8)()
    for i in range(8):
        for j in range(10):
            a[i][j] = randint(0, 255).to_bytes()
    return a

dev_info = DeviceInformation(randint(10000000, 100000000), int(time.time()))


client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(("localhost", 5000))

client.settimeout(100)

for count in range(100):
    random_image = Image(generate_random_image())
    gps = Geolocation(*[random() for _ in range(7)])
    msg_to_send = ImageAndGeolocation(gps,random_image, dev_info)
    client.sendall(
        packb(ExtType(2, bytes(msg_to_send)))
    )
    time.sleep(0.5)

