from bwmicroservice import BlackwingHandler
from bwmicroservice import BlackwingMicroservice
from dbmanager import DBManagerHandler

microservice = BlackwingMicroservice(configuration_path="/home/bwmicroservice/msdir/config.yaml", handler=DBManagerHandler)
microservice.start()