#!/bin/env python

from setuptools import setup, find_packages

# read the contents of your README file
from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="DBManager",
    version="0.0.1",
    author="Sergio Branco | The Architech",
    description="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    package_data={
        '': ['*.md'],
    },
    packages=['dbmanager'],
    package_dir={'': 'src'},
    license="GPLv3",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD 3-Clause License",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Topic :: Network"
    ],
)
